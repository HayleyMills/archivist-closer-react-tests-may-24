#!/usr/bin/env python3

"""
    - create a testing instrument with code list Yes/No and cs_dash
"""

import time
import sys
import logging
import os

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import mylib


test_instrument = mylib.test_instrument

def prepare_code_list(driver, test_instrument, t):
    """
    Add code list Yes/No and cs_dash to test_instrument
    """
    build_url = os.path.join(mylib.main_url, 'instruments', test_instrument, 'build')
    driver.get(build_url)

    # click on Code Lists
    driver.find_element(By.LINK_TEXT, 'Code Lists').click()
    time.sleep(t)

    # click on + NEW CODELIST
    driver.find_element(By.XPATH, '//span[text()="New CodeList"]').click()
    time.sleep(t)

    # New code list Yes/No
    inputLabel = driver.find_elements(By.XPATH, '//textarea[@name="label"]')[0]
    inputLabel.send_keys('Yes/No')

    # Codes + symbol
    # driver.find_element(By.XPATH, '//h3[text()="Codes"]/following-sibling::svg')
    driver.find_element(By.CSS_SELECTOR, 'svg.MuiSvgIcon-root:nth-child(6)').click()
    driver.find_element(By.CSS_SELECTOR, 'svg.MuiSvgIcon-root:nth-child(6)').click()

    # code 1
    driver.find_element(By.XPATH, '//textarea[@name="codes[0].value"]').send_keys('Yes')
    driver.find_element(By.XPATH, '//input[@name="codes[0].label"]').send_keys(1)
    # code 2
    driver.find_element(By.XPATH, '//textarea[@name="codes[1].value"]').send_keys('No')
    driver.find_element(By.XPATH, '//input[@name="codes[1].label"]').send_keys(0)

    # click on "Save"
    submiButton = driver.find_element(By.XPATH, '//span[text()="Save"]')
    submiButton.click()
    time.sleep(t)

    # click on + NEW CODELIST
    driver.find_element(By.XPATH, '//span[text()="New CodeList"]').click()
    time.sleep(t)

    # New code list
    inputLabel = driver.find_elements(By.XPATH, '//textarea[@name="label"]')[0]
    inputLabel.send_keys('cs_dash')

    # Codes + symbol
    driver.find_element(By.CSS_SELECTOR, 'svg.MuiSvgIcon-root:nth-child(6)').click()

    # code 1
    driver.find_element(By.XPATH, '//textarea[@name="codes[0].value"]').send_keys('-')
    driver.find_element(By.XPATH, '//input[@name="codes[0].label"]').send_keys('-')

    # click on "Save"
    submiButton = driver.find_element(By.XPATH, '//span[text()="Save"]')
    submiButton.click()
    time.sleep(t)


def prepare_test_instrument(uname, pw, t=5):
    """
    Add a test instrument
    """

    driver = mylib.get_driver()
    ok = mylib.archivist_login(driver, uname, pw)
    if not ok:
        print("could not login!")
        return

    try:
        print("login success")

        print("***** Create a new instrument *****")
        instrument_url = os.path.join(mylib.main_url, 'admin', 'instruments')
        driver.get(instrument_url)

        # click on "add new instrument" link
        driver.find_element(By.XPATH, '//a[@href="/instruments/new"]').click()
        time.sleep(t)

        # New instrument
        inputPrefix = driver.find_elements(By.XPATH, '//textarea[@name="prefix"]')[0]
        inputPrefix.send_keys(test_instrument)

        inputStudy = driver.find_elements(By.XPATH, '//textarea[@name="study"]')[0]
        inputStudy.send_keys('test_study')

        inputLabel = driver.find_elements(By.XPATH, '//textarea[@name="label"]')[0]
        inputLabel.send_keys('test_label')

        inputAgency = driver.find_elements(By.XPATH, '//textarea[@name="agency"]')[0]
        inputAgency.send_keys('CLOSER')

        inputVersion = driver.find_elements(By.XPATH, '//textarea[@name="version"]')[0]
        inputVersion.send_keys(1)

        # click on "Submit"
        submiButton = driver.find_element(By.XPATH, '//span[text()="Submit"]')
        submiButton.click()
        time.sleep(t)

        print("***** Create a new code list *****")
        prepare_code_list(driver, test_instrument, t)

    finally:
        # logout no matter how the code ended
        mylib.archivist_logout(driver)
        driver.quit()


def main():

    uname = sys.argv[1]
    pw = sys.argv[2]

    prepare_test_instrument(uname, pw)


if __name__ == "__main__":
    main()

