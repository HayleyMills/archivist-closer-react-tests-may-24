#!/usr/bin/env python3

"""
Python 3
    Web scraping using selenium to
    - add/delete code list
    - add/delete response domain (text, numeric, datetime)
    - add/delete question item
    - add/delete question grid
    - add/delete sequence
    - add/delete statement
    - add/delete condition
    - add/delete loop
"""

import time
import sys
import logging
import os

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import mylib

# log output
log = logging.getLogger("tester")

# use build page for "main_test_prefix"
test_study = mylib.test_instrument

def test_add_and_delete_code_list(uname, pw, t=5):
    """
    Add then delete a new code list
    """
    print("test_add_and_delete_code_list")
    log.info("\n-- test_add_and_delete_code_list")
    log.info("***** Create a new code list *****")
    driver = mylib.get_driver()
    ok = mylib.archivist_login(driver, uname, pw)
    if not ok:
        log.error("could not login!")
        return

    try:
        log.info("login success")

        build_url = os.path.join(mylib.main_url, 'instruments', test_study, 'build')
        driver.get(build_url)

        # click on Code Lists
        driver.find_element(By.LINK_TEXT, 'Code Lists').click()
        time.sleep(t)

        # New code list
        inputLabel = driver.find_elements(By.XPATH, '//textarea[@name="label"]')[0]
        inputLabel.send_keys('test_label')

        # response domain yes
        driver.find_elements(By.XPATH, '//input[@name="rd"]')[0].click()

        minResponses = driver.find_elements(By.XPATH, '//textarea[@name="min_responses"]')[0]
        minResponses.send_keys(1)

        maxResponses = driver.find_elements(By.XPATH, '//textarea[@name="max_responses"]')[0]
        maxResponses.send_keys(3)

        # click on "Save"
        submiButton = driver.find_element(By.XPATH, '//span[text()="Save"]')
        submiButton.click()
        time.sleep(t)

        try:
            msg = driver.find_element(By.XPATH, '//span[text()="test_label"]')
            success = True
            log.info("successfully created a new code list")
        except mylib.NoSuchElementException as e:
            success = False

        if not success:
            log.error("Could not find test_label: failed to create a new code list")
            return

        log.info("***** Delete a new code list *****")
        msg.click()
        time.sleep(t)

        # click on "Delete"
        deleteButton = driver.find_element(By.XPATH, '//span[text()="Delete"]')
        deleteButton.click()
        time.sleep(t)

        WebDriverWait(driver, t).until(EC.alert_is_present())
        driver.switch_to.alert.accept()

        time.sleep(t)
        try:
            driver.find_element(By.XPATH, '//span[text()="test_label"]')
            success = False
        except mylib.NoSuchElementException as e:
            success = True
            log.info("successfully deleted a new code list")

        if not success:
            log.error("Could find the test_label: failed delete a new code list")
            return

    finally:
        # logout no matter how the code ended
        mylib.archivist_logout(driver)
        driver.quit()


def test_add_and_delete_response_domain_text(uname, pw, t=5):
    """
    Add then delete a new response domain text
    """
    print("test_add_and_delete_response_domain_text")
    log.info("\n-- test_add_and_delete_response_domain_text")
    log.info("***** Create a new response domain text *****")
    driver = mylib.get_driver()
    ok = mylib.archivist_login(driver, uname, pw)
    if not ok:
        log.error("could not login!")
        return

    try:
        log.info("login success")

        build_url = os.path.join(mylib.main_url, 'instruments', test_study, 'build')
        driver.get(build_url)
        time.sleep(2*t)

        # click on ResponseDomains
        driver.find_element(By.LINK_TEXT, 'ResponseDomains').click()
        time.sleep(t)

        # New response domain: text
        driver.find_elements(By.XPATH, '//span[text()="Text"]')[0].click()
        time.sleep(t)

        inputLabel = driver.find_elements(By.XPATH, '//input[@name="label"]')[0]
        inputLabel.send_keys('test_label')

        inputLength = driver.find_elements(By.XPATH, '//input[@name="maxlen"]')[0]
        inputLength.send_keys('20')

        # click on "Save"
        submiButton = driver.find_element(By.XPATH, '//span[text()="Save"]')
        submiButton.click()
        time.sleep(t)

        try:
            msg = driver.find_element(By.XPATH, '//span[text()="test_label"]')
            success = True
            log.info("successfully created a new response domain text")
        except mylib.NoSuchElementException as e:
            success = False

        if not success:
            log.error("Could not find test_label: failed to create a new response domain text")
            # if not safe to continue?
            return

        log.info("***** Delete a new response domain text *****")
        msg.click()
        time.sleep(t)

        # click on "Delete"
        deleteButton = driver.find_element(By.XPATH, '//span[text()="Delete"]')
        deleteButton.click()
        time.sleep(2*t)

        WebDriverWait(driver, t).until(EC.alert_is_present())
        driver.switch_to.alert.accept()

        time.sleep(t)
        try:
            driver.find_element(By.XPATH, '//span[text()="test_label"]')
            success = False
        except mylib.NoSuchElementException as e:
            success = True
            log.info("successfully deleted a new response domain text")

        if not success:
            log.error("Could find the test_label: failed delete a new response domain text")
            return

    finally:
        # logout no matter how the code ended
        mylib.archivist_logout(driver)
        driver.quit()


def test_add_and_delete_response_domain_numeric(uname, pw, t=5):
    """
    Add then delete a new response domain numeric
    """
    print("test_add_and_delete_response_domain_numeric")
    log.info("\n-- test_add_and_delete_response_domain_numeric")
    log.info("***** Create a new response domain numeric *****")
    driver = mylib.get_driver()
    ok = mylib.archivist_login(driver, uname, pw)
    if not ok:
        log.error("could not login!")
        return

    try:
        log.info("login success")

        build_url = os.path.join(mylib.main_url, 'instruments', test_study, 'build')
        driver.get(build_url)

        # click on ResponseDomains
        driver.find_element(By.LINK_TEXT, 'ResponseDomains').click()
        time.sleep(t)

        # New response domain: Numeric
        driver.find_elements(By.XPATH, '//span[text()="Numeric"]')[0].click()
        time.sleep(t)

        inputLabel = driver.find_elements(By.XPATH, '//input[@name="label"]')[0]
        inputLabel.send_keys('test_label')

        # drop down
        driver.find_element(By.ID, 'mui-component-select-subtype').click()
        time.sleep(t)

        # select Date
        driver.find_element(By.XPATH, '//ul[@aria-labelledby="select-input-subtype"]//li[text()="Integer"]').click()
        time.sleep(t)

        inputMin = driver.find_elements(By.XPATH, '//input[@name="min"]')[0]
        inputMin.send_keys('1')

        inputMax = driver.find_elements(By.XPATH, '//input[@name="max"]')[0]
        inputMax.send_keys('100')

        # click on "Save"
        submiButton = driver.find_element(By.XPATH, '//span[text()="Save"]')
        submiButton.click()
        time.sleep(t)

        try:
            msg = driver.find_element(By.XPATH, '//span[text()="test_label"]')
            success = True
            log.info("successfully created a new response domain numeric")
        except mylib.NoSuchElementException as e:
            success = False

        if not success:
            log.error("Could not find test_label: failed to create a new response domain numeric")
            return

        log.info("***** Delete a new response domain numeric *****")
        # click on "Delete"
        msg.click()
        time.sleep(t)

        deleteButton = driver.find_element(By.XPATH, '//span[text()="Delete"]')
        deleteButton.click()
        time.sleep(t)

        WebDriverWait(driver, t).until(EC.alert_is_present())
        driver.switch_to.alert.accept()

        time.sleep(t)
        try:
            driver.find_element(By.XPATH, '//span[text()="test_label"]')
            success = False
        except mylib.NoSuchElementException as e:
            success = True
            log.info("successfully deleted a new response domain numeric")

        if not success:
            log.error("Could find the test_label: failed delete a new response domain numeric")
            return

    finally:
        # logout no matter how the code ended
        mylib.archivist_logout(driver)
        driver.quit()


def test_add_and_delete_response_domain_datetime(uname, pw, t=5):
    """
    Add then delete a new response domain datetime
    """
    print("test_add_and_delete_response_domain_datetime")
    log.info("\n-- test_add_and_delete_response_domain_datetime")
    log.info("***** Create a new response domain datetime *****")
    driver = mylib.get_driver()
    ok = mylib.archivist_login(driver, uname, pw)
    if not ok:
        log.error("could not login!")
        return

    try:
        log.info("login success")

        build_url = os.path.join(mylib.main_url, 'instruments', test_study, 'build')
        driver.get(build_url)

        # click on ResponseDomains
        driver.find_element(By.LINK_TEXT, 'ResponseDomains').click()
        time.sleep(t)

        # New response domain: Datetime
        driver.find_elements(By.XPATH, '//span[text()="Datetime"]')[0].click()
        time.sleep(t)

        inputLabel = driver.find_elements(By.XPATH, '//textarea[@name="label"]')[0]
        inputLabel.send_keys('test_label')
        time.sleep(t)

        # drop down
        driver.find_element(By.XPATH, '//div[@id="mui-component-select-subtype"]').click()
        time.sleep(t)

        # select Date
        driver.find_element(By.XPATH, '//ul[@aria-labelledby="select-input-subtype"]//li[text()="Date"]').click()
        time.sleep(t)

        # click on "Save"
        submiButton = driver.find_element(By.XPATH, '//span[text()="Save"]')
        submiButton.click()
        time.sleep(t)

        try:
            msg = driver.find_element(By.XPATH, '//span[text()="test_label"]')
            success = True
            log.info("successfully created a new response domain datetime")
        except mylib.NoSuchElementException as e:
            success = False

        if not success:
            log.error("Could not find test_label: failed to create a new response domain datetime")
            return

        log.info("***** Delete a new response domain datetime *****")
        # click on "Delete"
        msg.click()
        time.sleep(t)

        deleteButton = driver.find_element(By.XPATH, '//span[text()="Delete"]')
        deleteButton.click()
        time.sleep(t)

        WebDriverWait(driver, t).until(EC.alert_is_present())
        driver.switch_to.alert.accept()

        time.sleep(t)
        try:
            driver.find_element(By.XPATH, '//span[text()="test_label"]')
            success = False
        except mylib.NoSuchElementException as e:
            success = True
            log.info("successfully deleted a new response domain datetime")

        if not success:
            log.error("Could find the test_label: failed delete a new response domain datetime")
            return

    finally:
        # logout no matter how the code ended
        mylib.archivist_logout(driver)
        driver.quit()


def test_add_and_delete_question_item(uname, pw, t=5):
    """
    Add then delete a new question item
    """
    print("test_add_and_delete_question_item")
    log.info("\n-- test_add_and_delete_question_item")
    log.info("***** Create a new question item *****")
    driver = mylib.get_driver()
    ok = mylib.archivist_login(driver, uname, pw)
    if not ok:
        log.error("could not login!")
        return

    try:
        log.info("login success")

        build_url = os.path.join(mylib.main_url, 'instruments', test_study, 'build')
        driver.get(build_url)

        # click on question
        driver.find_element(By.LINK_TEXT, 'Questions').click()
        time.sleep(t)

        # New question
        inputLabel = driver.find_elements(By.XPATH, '//textarea[@name="label"]')[0]
        inputLabel.send_keys('test_label')

        inputInstruction = driver.find_elements(By.XPATH, '//textarea[@name="instruction"]')[0]
        inputInstruction.send_keys('test_instruction')

        inputLiteral = driver.find_elements(By.XPATH, '//textarea[@name="literal"]')[0]
        inputLiteral.send_keys('test_literal')

        # click on "Save"
        submiButton = driver.find_element(By.XPATH, '//span[text()="Save"]')
        submiButton.click()
        time.sleep(t)

        try:
            msg = driver.find_element(By.XPATH, '//span[text()="test_label"]')
            success = True
            log.info("successfully created a new question item")
        except mylib.NoSuchElementException as e:
            success = False

        if not success:
            log.error("Could not find test_label: failed to create a new question item")
            return

        log.info("***** Delete a new question item *****")
        # click on "Delete"
        msg.click()
        time.sleep(t)

        deleteButton = driver.find_element(By.XPATH, '//span[text()="Delete"]')
        deleteButton.click()
        time.sleep(t)

        WebDriverWait(driver, t).until(EC.alert_is_present())
        driver.switch_to.alert.accept()
        time.sleep(t)

        try:
            driver.find_element(By.XPATH, '//span[text()="test_label"]')
            success = False
        except mylib.NoSuchElementException as e:
            success = True
            log.info("successfully deleted a new question item")

        if not success:
            log.error("Could find the test_label: failed delete a new question item")
            return

    finally:
        # logout no matter how the code ended
        mylib.archivist_logout(driver)
        driver.quit()


def test_add_and_delete_question_grid(uname, pw, t=5):
    """
    Add then delete a new question grid
        - need to have code list cs_dash and Yes/No in place first
    """
    print("test_add_and_delete_question_grid")
    log.info("\n-- test_add_and_delete_question_grid")
    log.info("***** Create a new question grid *****")
    driver = mylib.get_driver()
    ok = mylib.archivist_login(driver, uname, pw)
    if not ok:
        log.error("could not login!")
        return

    try:
        log.info("login success")

        build_url = os.path.join(mylib.main_url, 'instruments', test_study, 'build')
        driver.get(build_url)

        # click on question
        driver.find_element(By.LINK_TEXT, 'Questions').click()
        time.sleep(t)

        # click on question grid
        driver.find_element(By.LINK_TEXT, "Question Grids").click()
        time.sleep(t)

        # New question
        inputLabel = driver.find_elements(By.XPATH, '//textarea[@name="label"]')[0]
        inputLabel.send_keys('test_label')

        inputInstruction = driver.find_elements(By.XPATH, '//textarea[@name="instruction"]')[0]
        inputInstruction.send_keys('test_instruction')

        inputLiteral = driver.find_elements(By.XPATH, '//textarea[@name="literal"]')[0]
        inputLiteral.send_keys('test_literal')

        # drop down horizontal
        driver.find_elements(By.XPATH, '//div[@id="mui-component-select-horizontal_code_list_id" and @role="button"]')[0].click()
        time.sleep(t)

        # select cs_dash
        driver.find_element(By.XPATH, '//ul[@aria-labelledby="select-input-horizontal_code_list_id"]//li[text()="cs_dash"]').click()
        time.sleep(t)

        # drop down vertical
        driver.find_elements(By.XPATH, '//div[@id="mui-component-select-vertical_code_list_id" and @role="button"]')[0].click()
        time.sleep(t)

        # select Yes/No
        driver.find_element(By.XPATH, '//ul[@aria-labelledby="select-input-vertical_code_list_id"]//li[text()="Yes/No"]').click()
        time.sleep(t)

        # click on "Save"
        submiButton = driver.find_element(By.XPATH, '//span[text()="Save"]')
        submiButton.click()
        time.sleep(t)

        try:
            msg = driver.find_element(By.XPATH, '//span[text()="test_label"]')
            success = True
            log.info("successfully created a new question grid")
        except mylib.NoSuchElementException as e:
            success = False

        if not success:
            log.error("Could not find test_label: failed to create a new question grid")
            return

        log.info("***** Delete a new question grid *****")
        msg.click()
        time.sleep(t)

        # Find this newly added code list then delete it
        driver.find_element(By.XPATH, '//span[text()="test_label"]').click()
        time.sleep(t)

        # click on "Delete"
        deleteButton = driver.find_element(By.XPATH, '//span[text()="Delete"]')
        deleteButton.click()
        time.sleep(t)

        WebDriverWait(driver, t).until(EC.alert_is_present())
        driver.switch_to.alert.accept()

        time.sleep(t)
        try:
            driver.find_element(By.XPATH, '//span[text()="test_label"]')
            success = False
        except mylib.NoSuchElementException as e:
            success = True
            log.info("successfully deleted a new question grid")

        if not success:
            log.error("Could find the test_label: failed delete a new question grid")
            return

    finally:
        # logout no matter how the code ended
        mylib.archivist_logout(driver)
        driver.quit()


def test_add_and_delete_sequence(uname, pw, t=5):
    """
    Add then delete a new sequence
    """
    print("test_add_and_delete_sequence")
    log.info("\n-- test_add_and_delete_sequence")
    log.info("***** Create a new sequence *****")
    driver = mylib.get_driver()
    ok = mylib.archivist_login(driver, uname, pw)
    if not ok:
        log.error("could not login!")
        return

    try:
        log.info("login success")

        build_url = os.path.join(mylib.main_url, 'instruments', test_study, 'build')
        driver.get(build_url)

        # click on Constructs
        driver.find_element(By.LINK_TEXT, 'Constructs').click()
        time.sleep(t)

        # '+' symbol
        driver.find_element(By.CLASS_NAME, "rst__toolbarButton").click()
        time.sleep(t)

        # select construct type
        driver.find_element(By.XPATH, '//span[text()="Click to select construct type"]').click()
        time.sleep(t)

        # new sequence
        driver.find_element(By.XPATH, '//span[text()="Sequence"]').click()
        time.sleep(t)

        inputLabel = driver.find_elements(By.XPATH, '//textarea[@name="label"]')[0]
        inputLabel.send_keys('test_label')

        # click on "Submit"
        submiButton = driver.find_element(By.XPATH, '//span[text()="Submit"]')
        submiButton.click()
        time.sleep(t)

        msg = driver.find_element(By.XPATH, '//span[text()="test_label"]')

        log.info("***** Delete a new sequence *****")
        # click on "Delete"
        msg.click()
        time.sleep(t)

        deleteButton = driver.find_element(By.XPATH, '//span[text()="Delete"]')
        deleteButton.click()
        time.sleep(t)

        WebDriverWait(driver, t).until(EC.alert_is_present())
        driver.switch_to.alert.accept()
        time.sleep(t)

        try:
            driver.find_element(By.XPATH, '//span[text()="test_label"]')
            success = False
        except mylib.NoSuchElementException as e:
            success = True
            log.info("successfully deleted a new sequence")

        if not success:
            log.error("Could find the test_label: failed delete a new sequence")
            return

    except Exception as e:
        log.error(f"failed with error: {str(e)}")
    finally:
        # logout no matter how the code ended
        mylib.archivist_logout(driver)
        driver.quit()


def test_add_and_delete_instrument(uname, pw, t=5):
    """
    Add then delete a new instrument
    """
    print("test_add_and_delete_instrument")
    log.info("\n-- test_add_and_delete_instrument")
    log.info("***** Create a new instrument *****")
    driver = mylib.get_driver()
    ok = mylib.archivist_login(driver, uname, pw)
    if not ok:
        log.error("could not login!")
        return

    try:
        log.info("login success")

        build_url = os.path.join(mylib.main_url, 'admin', 'instruments')
        driver.get(build_url)

        # click on "add new instrument" link
        driver.find_element(By.XPATH, '//a[@href="/instruments/new"]').click()
        time.sleep(t)

        # New instrument
        inputPrefix = driver.find_elements(By.XPATH, '//textarea[@name="prefix"]')[0]
        inputPrefix.send_keys('new_test_prefix')

        inputStudy = driver.find_elements(By.XPATH, '//textarea[@name="study"]')[0]
        inputStudy.send_keys('test_study')

        inputLabel = driver.find_elements(By.XPATH, '//textarea[@name="label"]')[0]
        inputLabel.send_keys('test_label')

        inputAgency = driver.find_elements(By.XPATH, '//textarea[@name="agency"]')[0]
        inputAgency.send_keys('test_agency')

        inputVersion = driver.find_elements(By.XPATH, '//textarea[@name="version"]')[0]
        inputVersion.send_keys('test_version')

        # click on "Submit"
        submiButton = driver.find_element(By.XPATH, '//span[text()="Submit"]')
        submiButton.click()
        time.sleep(t)

        try:
            msg = driver.find_element(By.XPATH, '//h1[text()="test_label"]')
            success = True
            log.info("successfully created a new instrument")
        except mylib.NoSuchElementException as e:
            success = False

        if not success:
            log.error("Could not find test_prefix: failed to create a new instrument")
            return

        log.info("***** Delete a new instrument *****")
        driver.get(build_url)
        time.sleep(t)

        # Find this newly added code list then delete it
        searchPrefix = driver.find_element(By.XPATH, "//input[@placeholder='Search by prefix (press return to perform search)']")
        searchPrefix.send_keys('test_prefix')

        # click on "Delete"
        deleteButton = driver.find_element(By.XPATH, '//span[text()="Delete"]')
        deleteButton.click()

        # type prefix
        searchPrefix = driver.find_element(By.XPATH, "//input[@id='outlined-error-helper-text']")
        searchPrefix.send_keys('test_prefix')
        time.sleep(t)

        # click on "Delete"
        # TODO: this fails b/c it finds the same one
        deleteElement = driver.find_element(By.XPATH, 'html/body/div/div/p/button/span[text()="Delete"]')

        if deleteElement.is_enabled():
            deleteElement.click()
        time.sleep(t)

        driver.get(build_url)
        time.sleep(t)
        try:
            driver.find_element(By.XPATH, '//td[text()="test_prefix"]')
            success = False
        except mylib.NoSuchElementException as e:
            success = True
            log.info("successfully deleted a new instrument")

        if not success:
            log.error("Could find the test_label: failed delete a new instrument")
            return

    finally:
        # logout no matter how the code ended
        mylib.archivist_logout(driver)
        driver.quit()


def test_add_and_delete_statement(uname, pw, t=5):
    """
    Add then delete a new statement
    - Does not create a statement with same name
    """
    print("test_add_and_delete_statement")
    log.info("\n-- test_add_and_delete_statement")
    log.info("***** Create a new statement *****")
    driver = mylib.get_driver()
    ok = mylib.archivist_login(driver, uname, pw)
    if not ok:
        log.error("could not login!")
        return

    try:
        log.info("login success")
        build_url = os.path.join(mylib.main_url, 'instruments', test_study, 'build')
        driver.get(build_url)

        # click on Constructs
        driver.find_element(By.LINK_TEXT, 'Constructs').click()
        time.sleep(t)

        # '+' symbol
        driver.find_element(By.CLASS_NAME, "rst__toolbarButton").click()
        time.sleep(t)

        # select construct type
        driver.find_element(By.XPATH, '//span[text()="Click to select construct type"]').click()
        time.sleep(t)

        # new statement
        driver.find_element(By.XPATH, '//span[text()="Statement"]').click()
        time.sleep(t)

        inputLabel = driver.find_element(By.XPATH, '//textarea[@name="label"]')
        inputLabel.send_keys('test_statement_label')
        inputLiteral = driver.find_element(By.XPATH, '//textarea[@name="literal"]')
        inputLiteral.send_keys('test_literal')

        # click on "Submit"
        submiButton = driver.find_element(By.XPATH, '//span[text()="Submit"]')
        submiButton.click()
        time.sleep(t)

        try:
            msg = driver.find_element(By.XPATH, '//span[text()="test_statement_label"]')
            success = True
            log.info("successfully created a new statement")
        except mylib.NoSuchElementException as e:
            success = False

        if not success:
            log.error("Could not find test_statement_label: failed to create a new statement")
            return

        log.info("***** Does not create a statement with same name *****")

        # '+' symbol
        driver.find_element(By.CLASS_NAME, "rst__toolbarButton").click()
        time.sleep(t)

        # select construct type
        driver.find_element(By.XPATH, '//span[text()="Click to select construct type"]').click()
        time.sleep(t)

        # new statement
        driver.find_element(By.XPATH, '//span[text()="Statement"]').click()
        time.sleep(t)

        driver.find_element(By.XPATH, '//textarea[@name="label"]').send_keys('test_statement_label')
        driver.find_element(By.XPATH, '//textarea[@name="literal"]').send_keys('test_literal')

        # click on "Submit"
        driver.find_element(By.XPATH, '//span[text()="Submit"]').click()
        time.sleep(t)

        try:
            errorMsg = driver.find_element(By.XPATH, '//div[text()="Error"]')
            errorM = True
            log.info("Already has the same name statement")
        except mylib.NoSuchElementException as e:
            errorM = False

        if not errorM:
            log.error("Could not find 'Error' message: failed to detect the same name statement")
        else:
            driver.find_elements(By.XPATH, "//*[name()='svg']")[-1].click()
            log.error("Stop trying to create a statement with existing name")

            # select construct type
            driver.find_element(By.XPATH, '//span[text()="Click to select construct type"]').click()
            time.sleep(t)

            # Delete
            driver.find_element(By.XPATH, '//span[text()="Delete"]').click()
            time.sleep(t)

        log.info("***** Delete a new statement *****")
        # click on "Delete"
        msg.click()
        time.sleep(t)

        deleteButton = driver.find_element(By.XPATH, '//span[text()="Delete"]')
        deleteButton.click()
        time.sleep(t)

        WebDriverWait(driver, t).until(EC.alert_is_present())
        driver.switch_to.alert.accept()
        time.sleep(t)

        try:
            driver.find_element(By.XPATH, '//span[text()="test_statement_label"]')
            success = False
        except mylib.NoSuchElementException as e:
            success = True
            log.info("successfully deleted a new statement")

        if not success:
            log.error("Could find the test_label: failed delete a new statement")
            return

    except Exception as e:
        log.error(f"failed with error: {str(e)}")
    finally:
        # logout no matter how the code ended
        mylib.archivist_logout(driver)
        driver.quit()


def test_add_and_delete_condition(uname, pw, t=5):
    """
    Add then delete a new condition
    """
    print("test_add_and_delete_condition")
    log.info("\n-- test_add_and_delete_condition")
    log.info("***** Create a new condition *****")
    driver = mylib.get_driver()
    ok = mylib.archivist_login(driver, uname, pw)
    if not ok:
        log.error("could not login!")
        return

    try:
        log.info("login success")
        build_url = os.path.join(mylib.main_url, 'instruments', test_study, 'build')
        driver.get(build_url)

        # click on Constructs
        driver.find_element(By.LINK_TEXT, 'Constructs').click()
        time.sleep(t)

        # '+' symbol
        driver.find_element(By.CLASS_NAME, "rst__toolbarButton").click()
        time.sleep(t)

        # select construct type
        driver.find_element(By.XPATH, '//span[text()="Click to select construct type"]').click()
        time.sleep(t)

        # new condition
        driver.find_element(By.XPATH, '//span[text()="Condition"]').click()
        time.sleep(t)

        inputLabel = driver.find_element(By.XPATH, '//textarea[@name="label"]')
        inputLabel.send_keys('test_condition_label')
        inputLiteral = driver.find_element(By.XPATH, '//textarea[@name="literal"]')
        inputLiteral.send_keys('test_literal')
        inputLogic = driver.find_element(By.XPATH, '//textarea[@name="logic"]')
        inputLogic.send_keys('if yes')

        # click on "Submit"
        submiButton = driver.find_element(By.XPATH, '//span[text()="Submit"]')
        submiButton.click()
        time.sleep(t)

        try:
            msg = driver.find_element(By.XPATH, '//span[text()="test_condition_label"]')
            success = True
            log.info("successfully created a new condition")
        except mylib.NoSuchElementException as e:
            success = False

        if not success:
            log.error("Could not find test_condition_label: failed to create a new condition")
            return

        log.info("***** Add a construct to true branch *****")
        # click on "+" next to the newly added condition true branch
        driver.find_element(By.XPATH, '//span[text()="test_condition_label True"]/parent::div[@class="rst__rowLabel"]/following-sibling::div').click()
        time.sleep(t)

        # select construct type
        driver.find_element(By.XPATH, '//span[text()="Click to select construct type"]').click()
        time.sleep(t)

        # new statement
        driver.find_element(By.XPATH, '//span[text()="Statement"]').click()
        time.sleep(t)

        trueLabel = driver.find_element(By.XPATH, '//textarea[@name="label"]')
        trueLabel.send_keys('true_statement_label')
        trueLiteral = driver.find_element(By.XPATH, '//textarea[@name="literal"]')
        trueLiteral.send_keys('test_literal')

        # click on "Submit"
        driver.find_element(By.XPATH, '//span[text()="Submit"]').click()
        time.sleep(2*t)

        try:
            msg_true = driver.find_element(By.XPATH, '//span[text()="true_statement_label"]')
            success = True
            log.info("successfully created a new statement inside the true branch of test_condition")
        except mylib.NoSuchElementException as e:
            success = False

        if not success:
            log.error("Could not find true_statement_label: failed to create a new statement inside the true branch of test_condition")
            return

        log.info("***** Add a construct to else branch *****")
        # click on "+" next to the newly added condition else branch
        driver.find_element(By.XPATH, '//span[text()="test_condition_label Else"]/parent::div[@class="rst__rowLabel"]/following-sibling::div').click()
        time.sleep(t)

        # select construct type
        driver.find_element(By.XPATH, '//span[text()="Click to select construct type"]').click()
        time.sleep(t)

        # new statement
        driver.find_element(By.XPATH, '//span[text()="Statement"]').click()
        time.sleep(t)

        elseLabel = driver.find_element(By.XPATH, '//textarea[@name="label"]')
        elseLabel.send_keys('else_statement_label')
        elseLiteral = driver.find_element(By.XPATH, '//textarea[@name="literal"]')
        elseLiteral.send_keys('test_literal')

        # click on "Submit"
        driver.find_element(By.XPATH, '//span[text()="Submit"]').click()
        time.sleep(t)

        try:
            msg_else = driver.find_element(By.XPATH, '//span[text()="else_statement_label"]')
            success = True
            log.info("successfully created a new statement inside the else branch of test_condition")
        except mylib.NoSuchElementException as e:
            success = False

        if not success:
            log.error("Could not find else_statement_label: failed to create a new statement inside the else branch of test_condition")
            return

        log.info("***** Delete a new condition *****")
        # click on "Delete"
        msg.click()
        time.sleep(t)

        deleteButton = driver.find_element(By.XPATH, '//span[text()="Delete"]')
        deleteButton.click()
        time.sleep(t)

        WebDriverWait(driver, t).until(EC.alert_is_present())
        driver.switch_to.alert.accept()
        time.sleep(t)

        try:
            driver.find_element(By.XPATH, '//span[text()="test_condition_label"]')
            success = False
        except mylib.NoSuchElementException as e:
            success = True
            log.info("successfully deleted a new condition")

        if not success:
            log.error("Could find the test_label: failed delete a new condition")
            return

    except Exception as e:
        log.error(f"failed with error: {str(e)}")
    finally:
        # logout no matter how the code ended
        mylib.archivist_logout(driver)
        driver.quit()


def test_add_and_delete_loop(uname, pw, t=5):
    """
    Add then delete a new loop
    """
    print("test_add_and_delete_loop")
    log.info("\n-- test_add_and_delete_loop")
    log.info("***** Create a new loop *****")
    driver = mylib.get_driver()
    ok = mylib.archivist_login(driver, uname, pw)
    if not ok:
        log.error("could not login!")
        return

    try:
        log.info("login success")
        build_url = os.path.join(mylib.main_url, 'instruments', test_study, 'build')
        driver.get(build_url)

        # click on Constructs
        driver.find_element(By.LINK_TEXT, 'Constructs').click()
        time.sleep(t)

        # '+' symbol
        driver.find_element(By.CLASS_NAME, "rst__toolbarButton").click()
        time.sleep(t)

        # select construct type
        driver.find_element(By.XPATH, '//span[text()="Click to select construct type"]').click()
        time.sleep(t)

        # new loop
        driver.find_element(By.XPATH, '//span[text()="Loop"]').click()
        time.sleep(t)

        inputLabel = driver.find_element(By.XPATH, '//textarea[@name="label"]')
        inputLabel.send_keys('test_loop_label')
        inputLoopVar = driver.find_element(By.XPATH, '//textarea[@name="loop_var"]')
        inputLoopVar.send_keys('Q1')
        inputStartVar = driver.find_element(By.XPATH, '//textarea[@name="start_val"]')
        inputStartVar.send_keys('Q2')

        # click on "Submit"
        submiButton = driver.find_element(By.XPATH, '//span[text()="Submit"]')
        submiButton.click()
        time.sleep(t)

        try:
            msg = driver.find_element(By.XPATH, '//span[text()="test_loop_label"]')
            success = True
            log.info("successfully created a new loop")
        except mylib.NoSuchElementException as e:
            success = False

        if not success:
            log.error("Could not find test_loop_label: failed to create a new loop")
            return

        log.info("***** Add a construct to a loop *****")
        # click on "+" next to the newly added loop
        driver.find_element(By.XPATH, '//span[text()="test_loop_label"]/parent::div[@class="rst__rowLabel"]/following-sibling::div').click()
        time.sleep(t)

        # select construct type
        driver.find_element(By.XPATH, '//span[text()="Click to select construct type"]').click()
        time.sleep(t)

        # new statement
        driver.find_element(By.XPATH, '//span[text()="Statement"]').click()
        time.sleep(t)

        inputLabel = driver.find_element(By.XPATH, '//textarea[@name="label"]')
        inputLabel.send_keys('loop_statement_label')
        inputLiteral = driver.find_element(By.XPATH, '//textarea[@name="literal"]')
        inputLiteral.send_keys('test_literal')

        # click on "Submit"
        submiButton = driver.find_element(By.XPATH, '//span[text()="Submit"]')
        submiButton.click()
        time.sleep(t)

        try:
            msg_statement = driver.find_element(By.XPATH, '//span[text()="loop_statement_label"]')
            success = True
            log.info("successfully created a new statement inside a loop")
        except mylib.NoSuchElementException as e:
            success = False

        if not success:
            log.error("Could not find loop_statement_label: failed to create a new statement inside a loop")
            return

        log.info("***** Delete a new loop *****")
        # click on "Delete"
        msg.click()
        time.sleep(t)

        deleteButton = driver.find_element(By.XPATH, '//span[text()="Delete"]')
        deleteButton.click()
        time.sleep(t)

        WebDriverWait(driver, t).until(EC.alert_is_present())
        driver.switch_to.alert.accept()
        time.sleep(t)

        try:
            driver.find_element(By.XPATH, '//span[text()="test_loop_label"]')
            success = False
        except mylib.NoSuchElementException as e:
            success = True
            log.info("successfully deleted a new loop")

        if not success:
            log.error("Could find the test_label: failed delete a new loop")
            return

    except Exception as e:
        log.error(f"failed with error: {str(e)}")
    finally:
        # logout no matter how the code ended
        mylib.archivist_logout(driver)
        driver.quit()


def main():

    uname = sys.argv[1]
    pw = sys.argv[2]

    from datetime import datetime

    nowstr = datetime.now().isoformat("_", "seconds").replace(":", "_")
    #logfile = f"mylog-{nowstr}.txt"
    logfile = "mylog.txt"
    if os.path.exists(logfile):
        os.unlink(logfile)

    print(f"Logging details to {logfile}")
    # format="%(asctime)s %(levelname)5s:%(name)s \t%(message)s",
    logging.basicConfig(
        format="%(levelname)5s:%(name)s \t%(message)s",
        #datefmt="%b%d %H:%M:%S %Z",
        filename=logfile,
    )
    # levels are DEBUG, INFO, WARNING, ERROR, CRITICAL
    logging.getLogger().setLevel("INFO")

    log.info("using uname %s", uname)
    log.debug("will not be seen by default")
    #log.error("some kinda of error happened")

    test_add_and_delete_response_domain_text(uname, pw)
    test_add_and_delete_response_domain_numeric(uname, pw)
    test_add_and_delete_response_domain_datetime(uname, pw)
    test_add_and_delete_code_list(uname, pw)
    test_add_and_delete_question_item(uname, pw)
    test_add_and_delete_question_grid(uname, pw)
    test_add_and_delete_sequence(uname, pw)
    test_add_and_delete_instrument(uname, pw)
    test_add_and_delete_statement(uname, pw)
    test_add_and_delete_condition(uname, pw)
    test_add_and_delete_loop(uname, pw)


if __name__ == "__main__":
    main()

