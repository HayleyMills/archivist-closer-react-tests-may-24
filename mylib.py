"""
Python 3
    Web scraping using selenium
"""

import time
import sys
import os
from urllib.parse import urlparse

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.chrome.options import Options as ChromeOptions


main_url = 'https://closer-build.herokuapp.com/'
# use name: main_test_prefix
test_instrument = 'main_test_prefix'

def get_driver():
    options = FirefoxOptions()
    # options.browser_version = '92'
    # options.platform_name = 'Windows 10'
    driver = webdriver.Remote(
        'http://selenium-standalone-firefox:4444/wd/hub',
        options=options)

    # locally use this instead:
    #driver = webdriver.Firefox()

    return driver


def archivist_login(driver, uname, pw, url=main_url, sleep_time=3):
    """
    Log in to archivist

    Returns:
        bool: True if the login seemed to work, False otherwise.
    """
    driver.get(url)
    time.sleep(sleep_time)

    try:
        driver.find_element(By.ID, "email").send_keys(uname)
        driver.find_element(By.ID, "password").send_keys(pw)
        driver.find_element(By.CLASS_NAME, "MuiButton-label").click()
    except NoSuchElementException as e:
        print(e)
        print("So we give up on this host")
        return False
    time.sleep(sleep_time)
    # check next page's title to make sure one is logged in
    if not driver.title.startswith("Archivist"):
        print("failed to login to this host, check your credentials?")
        return False
    return True


def archivist_logout(driver, sleep_time=3):
    """
    Log out from archivist
    """

    try:
        driver.find_element(By.CLASS_NAME, "MuiButton-label").click()
    except NoSuchElementException as e:
        print(e)
        return False
    time.sleep(sleep_time)
    # check next page's title to make sure one is logged out
    if not driver.title == "Archivist":
        print("failed to log out")
        return False
    return True


if __name__ == "__main__":
    raise RuntimeError("don't run this directly")


